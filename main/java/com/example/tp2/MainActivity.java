package com.example.tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    class SensorsList  extends BaseAdapter {
        List<SensorsData> dataSource;

        public SensorsList(List<SensorsData> data) {
            //super(MainActivity.this, R.layout.row, data);
            dataSource = data;
        }

        public void setData(List<SensorsData> d) {
            dataSource = d;
        }

        @Override
        public int getCount() {
            return dataSource.size();
        }

        @Override
        public Object getItem(int position) {
            return dataSource.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void updateCityList(List<SensorsData> newlist) {
            dataSource.clear();
            dataSource.addAll(newlist);
            this.notifyDataSetChanged();
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            SensorsData c = dataSource.get(position);
            convertView = LayoutInflater.from(MainActivity.this).inflate(R.layout.line,null);

            TextView nam= (TextView) convertView.findViewById(R.id.name);
            TextView val= (TextView) convertView.findViewById(R.id.validity);

            nam.setText(c.getName());
            val.setText(c.getValidity());
            return convertView;
        }
    }
    SensorsList SL=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<SensorsData> dLL= new ArrayList<>();
        ListView list = (ListView) findViewById(R.id.list);
        SensorManager mySensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);

        /*
        List<Sensor> sensorList = mySensorManager.getSensorList(Sensor.TYPE_ALL);


        for (Sensor sensor : sensorList) {
            if(sensor!=null)

            else
                dLL.add(new SensorsData(sensor.getName(),"false"));
        }**/
        List<Sensor> sensorList = mySensorManager.getSensorList(Sensor.TYPE_MAGNETIC_FIELD);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("MAGNETIC_FIELD","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_AMBIENT_TEMPERATURE);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("AMBIENT_TEMPERATURE","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_GAME_ROTATION_VECTOR);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("GAME_ROTATION_VECTOR","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("GEOMAGNETIC_ROTATION_VECTOR","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_GRAVITY);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("GRAVITY","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_GYROSCOPE);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("GYROSCOPE","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_GYROSCOPE_UNCALIBRATED);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE_UNCALIBRATED) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("GYROSCOPE_UNCALIBRATED","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_LIGHT);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_LIGHT) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("LIGHT","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_LINEAR_ACCELERATION  );
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION  ) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("LINEAR_ACCELERATION  ","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("MAGNETIC_FIELD_UNCALIBRATED","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_ORIENTATION);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("TYPE_ORIENTATION","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_PRESSURE);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("PRESSURE","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_PROXIMITY);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("PROXIMITY","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_RELATIVE_HUMIDITY);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("RELATIVE_HUMIDITY","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_ROTATION_VECTOR);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("ROTATION_VECTOR","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_SIGNIFICANT_MOTION);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_SIGNIFICANT_MOTION) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("SIGNIFICANT_MOTION","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_STEP_COUNTER);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("STEP_COUNTER","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_STEP_DETECTOR);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("STEP_DETECTOR","false"));

        sensorList = mySensorManager.getSensorList(Sensor.TYPE_TEMPERATURE);
        if(mySensorManager.getDefaultSensor(Sensor.TYPE_TEMPERATURE) != null)
            for(Sensor sensor : sensorList)
                dLL.add(new SensorsData(sensor.getName(),"true"));
        else
            dLL.add(new SensorsData("TEMPERATURE","false"));



        SL= new SensorsList(dLL);
        list.setAdapter(SL);


    }
}