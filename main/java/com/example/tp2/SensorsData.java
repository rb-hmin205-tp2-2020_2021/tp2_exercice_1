package com.example.tp2;

public class SensorsData {
    private String name;
    private String validity;
    public SensorsData(String n,String v)
    {
        name=n;
        validity=v;
    }
    public String getName() {
        return name;
    }
    public String getValidity()
    {
        return validity;
    }
}
